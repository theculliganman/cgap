#!/usr/bin/env python

#######################################################################################
#                                                                                     #
#  cGAP_MAIN.py                                                                       #
#    from cGAP_1.1 pipeline                                                           #
#                                                                                     #
#  Julian Egger                                                                       #
#  eggerj@ohsu.edu                                                                    #
#                                                                                     #
#  Omaha Zoo Genetics Department                                                      #
#  June 2015                                                                          #
#                                                                                     #
#                                                                                     #
#  This script runs the bulk of the work being done by cGAP.  Arguments are passed    #
#  from a slurm script created by start_cGAP.py.  The arguments include a list of     #
#  query gene files, a list of the query gene names, a list of both forward and       #
#  reverse short read files, a list of the common names between forward and reverse   #
#  read pairs, the user name of the Tusker user, and the full path of the current     #
#  working directory containing all cGAP scripts and input data.  The script creates  #
#  slurm scripts to run BLAST with each gene as the query and each sample as the      #
#  subject.  After BLAST is finished, another Slurm script is created to retrieve the #
#  fastq record headers of each read with a high blast hit to the query by means of   #
#  the script getBlastHits.pl.  After fastq records are retrieved, a Slurm script is  #
#  created to run makeCNS.py for each sample with the current group of query genes.   #
#                                                                                     #
#######################################################################################

#!/usr/bin/env python

import argparse
import sys, os
import subprocess as sp
import time


def slurmScript(fn, usr, path, slurmcmd, slurmjob, memSize, priority):
    """
    # Function containing template for creating slurm scripts
    """
    filename = fn + ".slurm"
    slurmFile = open(filename, "w")
    slurmFile.write("#!/bin/sh\n")
    slurmFile.write("#SBATCH --time=40:00:00\n")
    memory = "#SBATCH --mem-per-cpu=" + memSize + "\n"
    slurmFile.write(memory)
    if priority == 'y':
        partition = "#SBATCH --partition=hdzoo\n"
        slurmFile.write(partition)
    job = "#SBATCH --job-name=" + slurmjob + "\n"
    slurmFile.write(job)
    error = "#SBATCH --error=" + str(
        path) + "/logFiles_cGAP/" + slurmjob + ".%J.err\n"
    slurmFile.write(error)
    out = "#SBATCH --out=" + str(
        path) + "/logFiles_cGAP/" + slurmjob + ".%J.out\n"
    slurmFile.write(out)
    slurmFile.write(slurmcmd)
    slurmFile.write(
        '\necho -n "Max usage in bytes: " && cat /cgroup/memory/slurm/uid_${UID}/job_${SLURM_JOB_ID}/memory.max_usage_in_bytes\n')
    slurmFile.close()

def jobWait(jobName, usr, timer):
    """
    Waits a specified amount of time before allowing the next step of the
    process to begin.
    Inputs:
        jobName = Name of the slurm job that needs to be checked
        usr = username for the HCC computer that slurm jobs are under
        timer = the amount of time in seconds that should be waited.
    """

    cmd = 'squeue -u ' + usr + ' -n ' + jobName + ' -h'
    status = sp.check_output(cmd, shell=True)
    print "waiting for " + jobName
    while (len(status) > 0):
        #print "waiting for makeCNSJob"
        time.sleep(timer)
        status = sp.check_output(cmd, shell=True)


def copyCNSs(samples, queryNames, workPath):
    """
    # Function for copying consensus sequences from each sample to a single file
    # The single file represents one query gene and contains consensus sequences for all samples that
    # had an acceptable consensus sequence created
    """
    for s in samples:
        path = workPath + '/' + str(s) + '/cnsSeqs.' + str(s)  # location of all consensus sequences for each sample
        for filename in os.listdir(path):
            splitFile = filename.split(".")
            queryFile = 'cnsSamples/' + str(
                splitFile[0]) + '.sampleCNSs.' + str(splitFile[2])  # ex) cnsSamples/geneA.sampleCNSs.fa
            cmd = str('cat ' + path + '/' + filename + ' >> ' + queryFile)
            sp.call(cmd, shell=True)


def make_cnsSlurm(sampleNames, queryFiles, queryNames, usr, workPath,
                  priority):
    """
    # Function used for creating slurm script to run makeCNS.py script and
    create consensus sequences
    # make slurm script for each sample to create consensus sequences
    from query list
    """
    jobName = 'job_makeCNS'
    for s in sampleNames:
        slurmjob = jobName
        slurmcmd = 'cd ' + s + '\n'
        slurmcmd = slurmcmd + 'module load bwa/0.7\nmodule load compiler/gcc/4.8\nmodule load samtools/1.1\n'
        slurmcmd = slurmcmd + 'module load bcftools/1.2-4-g1fedb8b\nmodule load tabix/0.2\nmodule load python/2.7\nmodule load novocraft\n'
        slurmcmd = slurmcmd + 'python ' + workPath + '/makeCNS.py -sample ' + s + ' '
        slurmcmd = slurmcmd + '-files '
        for q in queryFiles:
            slurmcmd = slurmcmd + q + ' '
            slurmcmd = slurmcmd + '-names '
        for n in queryNames:
            slurmcmd = slurmcmd + n + ' '
        slurmcmd = slurmcmd + '-path ' + workPath
        filename = s + '/makeCNSs.' + s
        memSize = '1000'
        slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                    priority)
        cmd = str('sbatch ' + filename + '.slurm')
        sp.call(cmd, shell=True)

    jobWait(jobName, usr, 3)

def mergeReads(forwardReads, reverseReads, queryNames, samples):
    """
    # Function used to take reads from each sample with a high blast hit to
    each query gene and place
    # in a single file to be used for creating a consensus sequence.
    # All reads from forward reads file with high blast hit are written to a
    file (query.sample.FORWARD.fastq) and all reads
    # with high blast hit from reverse reads file are written to the same file
    (query.sample.FORWARD.fastq).  The headers from each
    # read record with blast hit is used to get the pair from the opposite
    reads file and placed in (query.sample.REVERSE.fastq).
    # Pair mate from forwards reads is written to (query.sample.REVERSE.fastq)
    and pair mate from reverse reads is also written
    # to (query.sample.REVERSE.fastq)
    """
    for f, r, s in zip(forwardReads, reverseReads, samples):
        fReads = f.split(".fastq")
        fReads = str(fReads[0])
        rReads = r.split(".fastq")
        rReads = str(rReads[0])
        for n in queryNames:
            # destinate files for reads
            mergedblasthitsFile = 'fr_readHits/' + n + '.' + s + '.FORWARD.fastq'  # file where reads with high blast hit will be placed
            mergedblastpairsFile = 'fr_readHits/' + n + '.' + s + '.REVERSE.fastq'  # file where mate of high blast hit reads will be placed
            # put blast hits from forward sample in file and their mate pairs in another file
            blasthitsFile = s + '/' + fReads + '/' + n + '.' + fReads + '.blasthits.fastq'
            blastpairsFile = s + '/' + fReads + '/' + n + '.' + fReads + '.hitpairs.fastq'
            cmd = str('cat ' + blasthitsFile + ' >> ' + mergedblasthitsFile)
            sp.call(cmd, shell=True)
            cmd = str('cat ' + blastpairsFile + ' >> ' + mergedblastpairsFile)
            sp.call(cmd, shell=True)
            # add reverse hits to same file as forward hits and reverse mate pairs to same file as forward mate pair hits
            blasthitsFile = s + '/' + rReads + '/' + n + '.' + rReads + '.blasthits.fastq'
            blastpairsFile = s + '/' + rReads + '/' + n + '.' + rReads + '.hitpairs.fastq'
            cmd = str('cat ' + blasthitsFile + ' >> ' + mergedblasthitsFile)
            sp.call(cmd, shell=True)
            cmd = str('cat ' + blastpairsFile + ' >> ' + mergedblastpairsFile)
            sp.call(cmd, shell=True)


def getHits(blastFile, min):
    """# Function called from make_getHitsSlurm()
       to create file containing a list of all fastq
       record headers that had a high blast alignment
       score to query gene
    """
    hits = []

    inFile = open(blastFile, "r")
    line = inFile.readline()
    found = False
    while (line):
        columns = line.split()
        if float(columns[11]) >= float(min):
            hits.append(columns[1])
        line = inFile.readline()

    fName = blastFile.split(".blast")
    fName = fName[0] + '.hits'
    outFile = open(fName, "w")
    """for fqselect2.pl"""
    for h in hits:
        outFile.write(h)
        outFile.write("\n")

    outFile.close()

    return fName


def make_getHitsSlurm(queryFiles, forwardReads, reverseReads, samples, usr,
                      workPath, queryNames, priority):
    """
    # Function for creating slurm script used to call getBlastHits.pl script
    and create a list
    # of all headers from read records with high blast hits to query gene
    """

    jobName = "job_getHits"

    for f, r, s in zip(forwardReads, reverseReads, samples):
        fReads = f.split(".fastq")
        fReads = str(fReads[0])
        rReads = r.split(".fastq")
        rReads = str(rReads[0])
        for n in queryNames:
            memSize = '30000'
            slurmjob = jobName
            """Create slurm script to get forward read blast hits and their reverse pairs using getBlastHits.py"""
            slurmcmd = 'cd ' + s + '/' + fReads + '\n'
            fBlastFile = s + '/' + fReads + '/' + fReads + '.' + n + '.blast'  # output file from blast
            hitsFile = workPath + '/' + getHits(fBlastFile, 50)
            slurmcmd = slurmcmd + 'perl ' + workPath + '/getBlastHits.pl ' + workPath + '/' + f + ' ' + hitsFile + ' ' + n + '.' + fReads + '.blasthits.fastq'
            filename = s + '/' + fReads + '/getblasthits.' + n + '.hits'
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)
            """2nd script to get hit pairs"""
            slurmcmd = 'cd ' + s + '/' + fReads + '\n'
            slurmcmd = slurmcmd + 'perl ' + workPath + '/getBlastHits.pl ' + workPath + '/' + r + ' ' + hitsFile + ' ' + n + '.' + fReads + '.hitpairs.fastq'
            filename = s + '/' + fReads + '/gethitpairs.' + n + '.hits'
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)
            """Create slurm script to get reverse read blast hits and their forward pairs using getBlastHits.py"""
            slurmcmd = 'cd ' + s + '/' + rReads + '\n'
            rBlastFile = s + '/' + rReads + '/' + rReads + '.' + n + '.blast'  #output file from blast
            hitsFile = workPath + '/' + getHits(rBlastFile, 50)
            slurmcmd = slurmcmd + 'perl ' + workPath + '/getBlastHits.pl ' + workPath + '/' + r + ' ' + hitsFile + ' ' + n + '.' + rReads + '.blasthits.fastq'
            filename = s + '/' + rReads + '/getblasthits.' + n + '.hits'
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)
            """2nd script to get hit pairs"""
            slurmcmd = 'cd ' + s + '/' + rReads + '\n'
            slurmcmd = slurmcmd + 'perl ' + workPath + '/getBlastHits.pl ' + workPath + '/' + f + ' ' + hitsFile + ' ' + n + '.' + rReads + '.hitpairs.fastq'
            filename = s + '/' + rReads + '/gethitpairs.' + n + '.hits'
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)

        jobWait(jobName, usr, 120)


def make_blastSlurm(queryFiles, forwardReads, reverseReads, samples, usr,
                    workPath, queryNames, priority):
    """# Function for creating slurm file to run blast with each sample of reads against each query gene cds sequence"""

    jobName = "jobBlast"

    for f, r, s in zip(forwardReads, reverseReads, samples):
        memSize = '12000'  # in megabytes, may need to be adjusted
        # Forward Reads
        reads = f.split(".fastq")
        reads = str(reads[0])
        for q, n in zip(queryFiles, queryNames):
            slurmjob = jobName
            slurmcmd = 'cd ' + s + '/' + reads + '\n'
            slurmcmd = slurmcmd + 'module load blast-legacy/2.2.26\n'
            slurmcmd = slurmcmd + 'blastall -p blastn -d ' + workPath + '/' + f + ' -i ' + workPath + '/' + q + ' -v 500000 -m 8 > ' + reads + '.' + n + '.blast\n'
            filename = s + '/' + reads + '/blast.' + reads + '.' + n
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)
        # Reverse Reads
        reads = r.split(".fastq")
        reads = str(reads[0])
        for q, n in zip(queryFiles, queryNames):
            slurmjob = jobName
            slurmcmd = 'cd ' + s + '/' + reads + '\n'
            slurmcmd = slurmcmd + 'module load blast-legacy/2.2.26\n'
            slurmcmd = slurmcmd + 'blastall -p blastn -d ' + workPath + '/' + r + ' -i ' + workPath + '/' + q + ' -v 500000 -m 8 > ' + reads + '.' + n + '.blast\n'
            filename = s + '/' + reads + '/blast.' + reads + '.' + n
            slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                        priority)
            cmd = str('sbatch ' + filename + '.slurm')
            sp.call(cmd, shell=True)

    jobWait(jobName, usr, 60)

def findMaxQueries(samples, variable):
    """
    # determine the number of query genes can be ran through cGAP pipeline at one time to keep number of Slurm jobs
    # below max amount of jobs on Tusker
    """
    return int(800 / (int(variable) * samples))


def runPipeline(queryFiles, queryNames, forwardReads, reverseReads,
                sampleNames, usr, workPath, priority):
    """
    # Function used to run main components of cGAP including blasting reads to reference genes, finding blast hit
    # read pairs, merging blast hits from both forward and reverse samples, and creating consensus sequences by using
    # open source software (bwa, samtools, novocraft, etc)
    """
    """
    STEP 1: BLAST
    # Deternine number of query files to be ran through pipeline at one time based
    # on number of samples inputted
    """
    maxQueries_BLAST = findMaxQueries(len(sampleNames), 2)

    qFiles = []
    qNames = []
    n = 0
    """Run cGAP in groups of query genes to keep total amount of slurm jobs running within max jobs limit for Tusker"""
    for f, name in zip(queryFiles, queryNames):
        qFiles.append(f)
        qNames.append(name)
        if n >= maxQueries_BLAST:  # Max number of jobs set to 902 at a time (900 gene against sample jobs plus start_cGAP.py Slurm and cGAP_MAIN.py Slurm)
            make_blastSlurm(qFiles, forwardReads, reverseReads, sampleNames,
                            usr, workPath, qNames, priority)
            qFiles = []
            qNames = []
            n = 0
        n = n + 1
    """Run cGAP on remaining genes from query genes directory"""
    if len(qFiles) != 0:
        make_blastSlurm(qFiles, forwardReads, reverseReads, sampleNames, usr,
                        workPath, qNames, priority)
    """Deternine number of query files to be ran through pipeline at one time based
    on number of samples inputted
    """
    maxQueries_getHits = findMaxQueries(len(sampleNames), 4)

    qFiles = []
    qNames = []

    n = 0
    """STEP 2 HITS: Run cGAP in groups of query genes to keep total amount of slurm jobs running within max jobs limit for Tusker
       (still need to check on Crane)
    """
    for f, name in zip(queryFiles, queryNames):
        qFiles.append(f)
        qNames.append(name)
        if n >= maxQueries_getHits:  # Max number of jobs set to 902 at a time (900 gene against sample jobs plus start_cGAP.py Slurm and cGAP_MAIN.py Slurm)
            make_getHitsSlurm(qFiles, forwardReads, reverseReads, sampleNames,
                              usr, workPath, qNames, priority)
            qFiles = []
            qNames = []
            n = 0
        n = n + 1
    """STEP 3 CONSENSUS:  STEP Run cGAP on remaining genes from query genes directory"""
    if len(qFiles) != 0:
        make_getHitsSlurm(qFiles, forwardReads, reverseReads, sampleNames, usr,
                          workPath, qNames, priority)
    """STEP"""
    mergeReads(forwardReads, reverseReads, queryNames, sampleNames)
    """STEP"""
    make_cnsSlurm(sampleNames, queryFiles, queryNames, usr, workPath, priority)

    return True


def main():
    """Main function used to take in arguments from slurm script created by start_cGAP script"""
    parser = argparse.ArgumentParser(
        description=
        'cGAP: Main Program for Creating Consenses Sequences in the Consensus Gene Assembly Program')

    parser.add_argument('-q',
                        action="store",
                        dest="q",
                        nargs="+")  # Query Files
    parser.add_argument('-names',
                        action="store",
                        dest="names",
                        nargs="+")  # Query Names
    parser.add_argument('-forward',
                        action="store",
                        dest="forward",
                        nargs="+")  # Forward Short Read Files
    parser.add_argument('-reverse',
                        action="store",
                        dest="reverse",
                        nargs="+")  # Reverse Short Read Files
    parser.add_argument('-samples',
                        action="store",
                        dest="samples",
                        nargs="+")  # Sample Names
    parser.add_argument('-usr', action="store", dest="usr")  # Tusker User Name
    parser.add_argument('-path',
                        action="store",
                        dest="path")  # Full path for working directory
    parser.add_argument('-priority',
                        action="store",
                        dest="priority")  # Option for having Slurm scripts work on priority access

    exCommand = "Example: python cGAP.py -q geneFiles/FCGR1A.fa -names FCGR1A -forward a1.gz -reverse a2.gz -samples a -usr jaegger -path /work/hdzoo/jaegger/cGAP -priority n"

    if len(sys.argv) < 2:
        parser.print_usage()
        print exCommand
        sys.exit(1)

    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))

    queryFiles = args.q
    queryNames = args.names
    forwardReads = args.forward
    reverseReads = args.reverse
    sampleNames = args.samples
    usr = args.usr
    workPath = args.path
    priority = args.priority
    """Check to see if arguments were passed to cGAP script correctly"""
    if queryFiles == None:
        print "\nNo query files entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if queryNames == None:
        print "\nNo query names entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if forwardReads == None:
        print "\nNo forward reads files entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if reverseReads == None:
        print "\nNo reverse reads files enetered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if sampleNames == None:
        print "\nNo sample names entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if usr == None:
        print "\nNo user name entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if workPath == None:
        print "\nNo path to working directory entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if priority == None:
        print "\nNo option for priority access entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    # Send arguments to run cGAP function
    runPipeline(queryFiles, queryNames, forwardReads, reverseReads,
                sampleNames, usr, workPath, priority)


main()
