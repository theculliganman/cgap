#!/usr/bin/env python

###########################################################
#                                                         #
#  makeCNS.py                                             #
#     from cGAP_1.1 pipeline                              #
#                                                         #
#  Written by:                                            #
#     Julian A. Egger                                     #
#     Omaha Zoo Genetics Department                       #
#     June 2015                                           #
#                                                         #
#  This script is ran from a Slurm script created         #
#  by cGAP_MAIN.py. It is used to call external software  #
#  including BWA, Samtools, BCFtools, Novocraft           #
#  and create consensus CDS sequences from                #
#  reference query genes for each sample of reads.        #
#  Consensus sequences contain an 'N' for every           #
#  position in which no reads align to the reference.     #
#                                                         #
#  Contact:                                               #
#     Julian Egger                                        #
#     eggerj@ohsu.edu                                     #
#                                                         #
###########################################################

import subprocess as sp
from Bio import SeqIO
import argparse
import sys, os
import time


def getPositions(depthFile):
    """
    # Used to get the positions that have coverage from the newly created consensus sequence
    # by accessing the depth file created earlier.
    """
    positions = []

    inFile = open(depthFile, "r")
    line = inFile.readline()
    while (line):
        splitline = line.split("\t")
        pos = int(splitline[0])
        positions.append(pos)
        line = inFile.readline()

    return positions


def maskSeq(s, n, unMasked, depthFile):
    """# Used to replace all regions of the consensus sequence that did not have any
    # reads aligned to it with N's in those respective nucleotide positions
    """

    cnsFile = 'cnsSeqs.' + str(s) + '/' + str(n) + '.cns.fasta'

    positions = getPositions(depthFile)

    for rec in SeqIO.parse(unMasked, "fasta"):
        id = str(rec.id)
        seq = str(rec.seq)

    maskedSeq = ''

    n = 1
    for nuc in seq:
        if n in positions:
            maskedSeq = maskedSeq + nuc
        else:
            maskedSeq = maskedSeq + 'N'
        n = n + 1

    maskedFile = open(cnsFile, "w")
    maskedFile.write(">" + id + " Sample: " + s + "\n")
    maskedFile.write(maskedSeq + "\n")

    maskedFile.close()


def copy2workDirectory(f, path):
    """
    # Makes a copy of the reference file to the directory 'queryWork' so all
    # index files can be placed there without interfering with the original
    # query gene directory which is being parsed for reference genes.
    """
    refFile = str(path) + '/' + str(f)
    fName = refFile.split("/")
    fName = fName[-1]
    cmd = 'cp ' + refFile + ' ' + '../queryWork/' + fName
    sp.call(cmd, shell=True)
    newRef = str(path) + '/queryWork/' + str(fName)

    return str(newRef)


def cnsCommands(s, f, n, path):
    """
    # Function used to run external software commands for creating consensus sequences
    # and masking regions where no reads align
    """
    refFile = copy2workDirectory(f, path)
    fReads = '../fr_readHits/' + str(n) + '.' + str(s) + '.FORWARD.fastq'
    rReads = '../fr_readHits/' + str(n) + '.' + str(s) + '.REVERSE.fastq'
    unMasked = str(n) + '.unmasked.fasta'
    depthFile = str(n) + '.depth.txt'

    # Load Modules
    cmd = 'module load blast-legacy/2.2.26'
    sp.call(cmd, shell=True)
    cmd = 'module load bwa/0.7'
    sp.call(cmd, shell=True)
    cmd = 'module load compiler/gcc/4.8'
    sp.call(cmd, shell=True)
    cmd = 'module load samtools/1.1'
    sp.call(cmd, shell=True)
    cmd = 'module load bcftools/1.2-4-g1fedb8b'
    sp.call(cmd, shell=True)
    cmd = 'module load tabix/0.2'
    sp.call(cmd, shell=True)
    cmd = 'module load python/2.7'
    sp.call(cmd, shell=True)
    cmd = 'module load novocraft'
    sp.call(cmd, shell=True)

    # NEW COMMANDS WITH PIPING

    #bwa index CREBBP.fa
    cmd = 'bwa index ' + refFile
    sp.call(cmd, shell=True)

    #samtools faidx CREBBP.fa
    cmd = 'samtools faidx ' + refFile
    sp.call(cmd, shell=True)

    # Create consensus sequence without having regions with no read alignment accounted for
    cmd = 'bwa mem -t 4 ' + refFile + ' ' + fReads + ' ' + rReads + ' | samtools view -Su - | novosort -m 1g -t . - | samtools mpileup -A -ug -f ' + refFile + ' - | bcftools call -c -A -O z | gunzip | grep -v -P  $"\.\t[ACGT]\t\." | novoutil iupac -g -q 30 - ' + refFile + ' > ' + unMasked
    sp.call(cmd, shell=True)

    # Rerun first few commands to get an output depth file used for masking regions with no read alignment
    cmd = "bwa mem -t 4 " + refFile + " " + fReads + " " + rReads + " | samtools view -Su - | novosort -m 1g -t . - | samtools mpileup -A -ug -f " + refFile + " - | bcftools query -f '%POS\\t%DP\\n' -o " + depthFile
    sp.call(cmd, shell=True)

    maskSeq(s, n, unMasked, depthFile)


def runCommands(sample, files, names, workPath):
    """# Function used to parse through gene list and send parameters to cnsCommands"""
    for f, n in zip(files, names):
        cnsCommands(sample, f, n, workPath)


def main():
    """# Main function use to take in arguments and check for errors."""
    parser = argparse.ArgumentParser(
        description='cGAP: Consensus Gene Assembly Program')

    parser.add_argument('-sample',
                        action="store",
                        dest="sample",
                        type=str)  # Sample
    parser.add_argument('-files',
                        action="store",
                        dest="files",
                        nargs="+")  # Query Files
    parser.add_argument('-names',
                        action="store",
                        dest="names",
                        nargs="+")  # Query Names
    parser.add_argument('-path',
                        action="store",
                        dest="path",
                        type=str)  # Path of Query Files

    exCommand = "Example: python cGAP.py -sample a -files VN1R4.fa SET.fa -names VN1R4 SET -path /work/hdzoo/jaegger/cGAP"

    if len(sys.argv) < 2:
        parser.print_usage()
        print exCommand
        sys.exit(1)

    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))

    sample = args.sample
    queryFiles = args.files
    queryNames = args.names
    workPath = args.path

    if sample == None:
        print "\nNo sample name entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if queryFiles == None:
        print "\nNo query files entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if queryNames == None:
        print "\nNo query names entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if workPath == None:
        print "\nNo work path entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    # Send sample name, query files/names, and full path to runCommands function.
    runCommands(sample, queryFiles, queryNames, workPath)


main()
