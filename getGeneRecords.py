from Bio import SeqIO

# TWO OUTPUT TYPES:
#   Individual gene CDS files containing only the individual sequence placed in specified directory
#   A file containing all reference CDS files

refFile = open("Homo_Sapiens.CDS.protein_coding.fasta", "w")  # File to hold all reference CDS records

for rec in SeqIO.parse("Homo_sapiens.GRCh38.cds.all.fa", "fasta"):  # Reference CDS file
    d = str(rec.description)  # header
    splitRec = d.split(" ")  # split header by spaces to separate fields
    type = splitRec[1]  # field 1 (0 based)
    splitType = type.split(":")  # split type by : to look for known or unkown
    seqLength = len(str(rec.seq))  # determine length of sequence and use minimum length as a filter
    # Some filtering is applied in the next line.  May not be needed as BioMart already allows for these filters when downloading
    # a text file containg gene and transcript ensembl IDs.  Also want to include gene names with BioMart download
    if splitType[1] == 'known' and splitRec[
            4
    ] == 'gene_biotype:protein_coding' and splitRec[
            5
    ] == 'transcript_biotype:protein_coding' and seqLength >= 100:
        eGID = splitRec[3].split(":")  # split field 3 (0 based) to get ensembl geneID
        eGID = eGID[1]  # ensembl geneID
        martFile = open("mart_export.txt", "r")  # name of BioMart file containing gene ID and transcript ID
        line = martFile.readline()
        while (line):
            splitLine = line.split("\t")  # split line by tab to separate gene ID, transcript ID, and gene name
            if splitLine[0] == eGID and splitLine[1] == splitRec[0]:  # if gene ID and transcript ID of record match IDs in BioMart file
                gID = splitLine[2]  # use field 2 (0 based) for gene ID and naming file to contain CDS record
                gID = gID.split("\n")
                gID = gID[0]
                filename = 'new_geneFiles/' + gID + '.fa'
                newFile = open(filename, "w")  # file for reference CDS sequence
                newFile.write(">" + d + "\n")  # write header to gene CDS file
                newFile.write(str(rec.seq))  # write sequence to gene CDS file
                newFile.close()
                refFile.write(">" + d + "\n")  # write CDS record to file containg all reference CDS records
                refFile.write(str(rec.seq) + "\n")
            line = martFile.readline()
        martFile.close()

refFile.close()
