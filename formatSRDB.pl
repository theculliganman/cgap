#!/usr/bin/perl

####################################################
#                                                  #
#  Perl script originally created                  #
#  by Richard Bankoff of Penn                      #
#  State University.  This script                  #   
#  is used to pipe headers and sequences           #
#  of each fastq record into the formatdb          #
#  command of BLAST in order to format the         #
#  subject database to be used when blasting       #
#  each query gene to the sample reads.            #
#                                                  #
#  Contact Information:                            #
#     Julian Egger                                 #
#     eggerj@ohsu.edu                              # 
#                                                  #
#     Richard Bankoff                              #
#     richardbankoff@gmail.com                     #
#                                                  #  
#                                                  #
####################################################

my $SRG = $ARGV[0];
my $SR = $SRG;
$SR =~ s/.gz//g;


open($fh, sprintf("zcat %s |", $SRG)) or die "Broken gunzip $!\n";


open ($fh2, "| formatdb -i stdin -n $SRG -p F") or die "no piping formatdb!, $!\n";

			
#Fastq => Fasta sub
my $localcounter = 0;
while (my $line = <$fh>){
	if ($. % 4==1){
		print $fh2 "\>" . substr($line, 1);
		$localcounter++;
	}
	elsif ($localcounter == 1){
		print $fh2 "$line";
		$localcounter = 0;
	}
	else{
	}
}
close $fh;
close $fh2;
			
exit;
