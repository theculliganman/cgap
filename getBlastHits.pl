#!/usr/bin/perl

###########################################
#                                         #
#  getBlastHits.pl                        #
#  cGAP_1.1                               #
#  Julian Egger                           #                                     
#                                         #
#  Perl script originally created         #
#  by Richard Bankoff of Penn             #
#  State University.  This script         #   
#  is used get fastq records from         # 
#  inputted reads file based on a         #
#  file of record IDs and write them      #
#  to a new file to be used later when    #
#  generating the consensus sequence      #
#  for a given query gene and sample.     #
#                                         #                          
#  Contact Information:                   #
#      Julian Egger                       #
#      eggerj@ohsu.edu                    # 
#                                         #
#      Richard Bankoff                    #
#      richardbankoff@gmail.com           #
#                                         #
#                                         #
###########################################


use strict;
use warnings;

my $in = $ARGV[0];	#whole fastq file
my $keep = $ARGV[1];	#list of IDs to keep 
my $out = $ARGV[2];	#outfile, new fastq

unless (@ARGV==3) {
	die "\nRequired: file.fastq (no weird wrapping please), list of IDs to keep, outfile\n\n";
}

unless (-f $in) {
	die "\nCouldn't find $in\n\n";
}

unless (-f $keep) {
	die "\nCouldn't find $keep\n\n";
}


# Get list of IDs
my @ids;
open KEEP, "$keep";
while (<KEEP>) {
    my $head = $_;
    my $id = (split(/\s{1,}/,$head))[0];
    my $newid = "@" . $id;
    push @ids, $newid;
}


close KEEP;

my $gz = 0;
if ($in =~ /.gz$/) {
	$gz = 1;
}

if ($gz==1) {
        open(IN, sprintf("zcat %s |", $in));
}

else {
	open IN, $in;
}


# This block finds the records matching the IDs from input file and writes those
# records to a new fastq file
my $NR;
open OUT, ">$out";
while (<IN>) {
	$NR++;
	my $line = $_;
	chomp $line;
	if ($NR%4==1) {
		if ($line !~ /^\@/) {
			die "\nERROR!! Your fastq file is truncated, squelched, or wrapped at line $NR, please fix it and try again...\n\n";
		}
		my $header = substr $line, 1;
                my $fqid = (split(/\s{1,}/,$line))[0];
                if (grep $_ eq $fqid, @ids) { 
			print OUT "\@$header\n";
			my $i = 0;
			until ($i==3) {
				my $rec = <IN>;
				chomp $rec;
				print OUT "$rec\n";
				$i++;
				$NR++;
			}
		}
	}
}
