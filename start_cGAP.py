#!/usr/bin/env python

import sys, os
import argparse
import subprocess as sp
import time


def slurmScript(fn, usr, path, slurmcmd, slurmjob, memSize, priority):
    """
    # Function containing template for slurm scripts
    """

    filename = fn + ".slurm"
    slurmFile = open(filename, "w")
    slurmFile.write("#!/bin/sh\n")
    slurmFile.write("#SBATCH --time=140:00:00\n")
    memory = "#SBATCH --mem-per-cpu=" + memSize + "\n"
    slurmFile.write(memory)
    if priority == 'y':
        partition = "#SBATCH --partition=hdzoo\n"
        slurmFile.write(partition)
    job = "#SBATCH --job-name=" + slurmjob + "\n"
    slurmFile.write(job)
    error = "#SBATCH --error=" + str(
        path) + "/logFiles_cGAP/" + slurmjob + ".%J.err\n"
    slurmFile.write(error)
    out = "#SBATCH --out=" + str(
        path) + "/logFiles_cGAP/" + slurmjob + ".%J.out\n"
    slurmFile.write(out)
    slurmFile.write(slurmcmd)
    slurmFile.write(
        '\necho -n "Max usage in bytes: " && cat /cgroup/memory/slurm/uid_${UID}/job_${SLURM_JOB_ID}/memory.max_usage_in_bytes\n')
    slurmFile.close()


def make_cGAPSlurm(cgapCMD, usr, workPath, priority):
    """
    # Function for creating slurm script for running cGAP pipeline
    """
    slurmjob = 'job_cGAP'
    filename = 'slurmGAP'
    memSize = '1000'
    slurmScript(filename, usr, workPath, cgapCMD, slurmjob, memSize, priority)
    cmd = str('sbatch ' + filename + '.slurm')
    sp.call(cmd, shell=True)

    # Wait until Slurm scripts are done runnning blast
    cmd = 'squeue -u ' + usr + ' -n job_cGAP -h'
    status = sp.check_output(cmd, shell=True)
    while (len(status) > 0):
        #print "waiting for cGAP job"
        time.sleep(10)
        status = sp.check_output(cmd, shell=True)


def make_formatDBSlurm(forwardReads, reverseReads, usr, workPath, priority):
    """
    # Function used to create slurm scripts for running formatdb on read files to create databases for blast
    """
    for f, r in zip(forwardReads, reverseReads):
        memSize = '45000'
        # Use slurmScript to create slurm script for running formatSRDB.py
        # and create database for each short read file
        slurmjob = 'job_fSRDB'
        # Forward Reads
        slurmcmd = 'perl formatSRDB.pl ' + f
        sample = f.split(".fastq")
        filename = str(sample[0])
        slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                    priority)
        # Run slurm script for forward reads
        cmd = str('sbatch ' + filename + '.slurm')
        sp.call(cmd, shell=True)
        # Reverse reads
        slurmcmd = 'perl formatSRDB.pl ' + r
        sample = r.split(".fastq")
        filename = str(sample[0])
        slurmScript(filename, usr, workPath, slurmcmd, slurmjob, memSize,
                    priority)
        # Run slurm script for reverse reads
        cmd = str('sbatch ' + filename + '.slurm')
        sp.call(cmd, shell=True)

    # Wait until Slurm scripts are done runnning blast
    cmd = 'squeue -u ' + usr + ' -n job_fSRDB -h'
    status = sp.check_output(cmd, shell=True)
    while (len(status) > 0):
        print "waiting for formatSRDB job"
        time.sleep(600)
        status = sp.check_output(cmd, shell=True)


def make_SampleDirectories(sampleNames, forwardReads, reverseReads):
    """
    # Function used to create directories for storing and organizing all files created by cGAP
    """
    for s in sampleNames:
        # Make a directory for each sample (forward and reverse reads)
        cmd = str('mkdir -p ' + s)
        sp.call(cmd, shell=True)
        # Make a subdirectory in each new sample directory for consensus cds sequences
        cmd = str('mkdir -p ' + s + '/cnsSeqs.' + s)
        sp.call(cmd, shell=True)

    for s, f, r in zip(sampleNames, forwardReads, reverseReads):
        # Create subdirectory in sample directory named after forwardshort reads file w/o extensions
        sample = f.split(".fastq")
        dirname = str(sample[0])
        cmd = str('mkdir -p ' + s + '/' + dirname)
        sp.call(cmd, shell=True)
        # Create subdirectory in sample directory named after forwardshort reads file w/o extensions
        sample = r.split(".fastq")
        dirname = str(sample[0])
        cmd = str('mkdir -p ' + s + '/' + dirname)
        sp.call(cmd, shell=True)

    cmd = str('mkdir -p  cnsSamples')
    sp.call(cmd, shell=True)
    cmd = str('mkdir -p fr_readHits')
    sp.call(cmd, shell=True)
    cmd = str('mkdir -p queryWork')
    sp.call(cmd, shell=True)
    cmd = str('mkdir -p logFiles_cGAP')
    sp.call(cmd, shell=True)


def copyCNSs(samples, queryNames, workPath):
    """
    # function used to copy consensus sequences from each sample to a single file for each query
    """
    for s in samples:
        path = workPath + '/' + str(s) + '/cnsSeqs.' + str(s)  # ex) /home/hdzoo/jaegger/cGAP/sample1/cnsSeqs.sample1
        for filename in os.listdir(path):
            splitFile = filename.split(".")
            queryFile = 'cnsSamples/' + str(
                splitFile[0]) + '.sampleCNSs.' + str(splitFile[2])  #ex) cnsSamples/geneA.sampleCNSs.fa
            cmd = str('cat ' + path + '/' + filename + ' >> ' + queryFile)
            sp.call(cmd, shell=True)


def start(directory, forwardReads, reverseReads, sampleNames, usr, workPath,
          formatDB, priority):
    """
    # Take in directory of gene query files, forward read files, reverse read files, commond name
    # of read files, Tusker user name, path of cGAP scripts, read files, and gene queries directory
    """
    # Make directories for containing files created by cGAP
    make_SampleDirectories(sampleNames, forwardReads, reverseReads)

    if formatDB == 'y':
        # Call to function to create slurm scripts for formatting read databases for blast
        make_formatDBSlurm(forwardReads, reverseReads, usr, workPath, priority)

    fReadList = ''  # store names of forward read files
    rReadList = ''  # store names of reverse read files
    sNamesList = ''  # store common names of read pairs
    for f, r, s in zip(forwardReads, reverseReads, sampleNames):
        fReadList = fReadList + f + ' '
        rReadList = rReadList + r + ' '
        sNamesList = sNamesList + s + ' '

    geneFileList = []  # list of query files
    geneNameList = []  # list of query names taken from file names

    # get names of query genes from query file names
    # name is determined as the group of characters before first "." in file name
    path = workPath + '/' + directory
    for filename in os.listdir(path):
        geneFile = directory + '/' + str(filename)
        geneName = filename.split(".")
        geneName = geneName[0]
        geneFileList.append(geneFile)
        geneNameList.append(geneName)

    queryNames = ''  # string for holding query names to be used as argument for cGAP
    queryFiles = ''  # string for holding query file names to be used as argument for cGAP
    n = 0
    # Run cGAP in groups of query genes to keep total amount of slurm jobs running within max jobs limit for Tusker
    for f, name in zip(geneFileList, geneNameList):
        queryFiles = queryFiles + f + ' '
        queryNames = queryNames + name + ' '
        n = n + 1

    # Run cGAP on remaining genes from query genes directory
    cmd = "python cGAP_MAIN.py -q " + queryFiles + " -names " + queryNames + " -forward " + fReadList + " -reverse " + rReadList + " -samples " + sNamesList + " -usr " + usr + " -path " + workPath + " -priority " + priority
    make_cGAPSlurm(cmd, usr, workPath, priority)

    # copy consensus sequences made from each sample to single file for each query gene
    copyCNSs(sampleNames, geneNameList, workPath)


# Main function used to take in arguments and check for errors.
def main():

    parser = argparse.ArgumentParser(
        description=
        'Start cGAP: Script for running Consensus Gene Assembly Program')

    parser.add_argument('-directory',
                        action="store",
                        dest="directory")  # Directory of query files
    parser.add_argument('-forward',
                        action="store",
                        dest="forward",
                        nargs="+")  # Forward Short Read Files
    parser.add_argument('-reverse',
                        action="store",
                        dest="reverse",
                        nargs="+")  # Reverse Short Read Files
    parser.add_argument('-samples',
                        action="store",
                        dest="samples",
                        nargs="+")  # Sample Names
    parser.add_argument('-usr', action="store", dest="usr")  # Tusker User Name
    parser.add_argument('-path',
                        action="store",
                        dest="path")  # Full path for working directory
    parser.add_argument('-formatdb',
                        action="store",
                        dest="formatdb")  # Option for formatting database or not
    parser.add_argument('-priority',
                        action="store",
                        dest="priority")  # Option for having Slurm scripts work on priority access

    exCommand = "Example: python start_cGAP.py -directory geneFiles -forward a1.fastq.gz t1.fastq.gz -reverse a2.fastq.gz t2.fastq.gz -samples a t -usr jaegger -path /work/hdzoo/jaegger/cGAP -formatdb y -priority n"

    if len(sys.argv) < 2:
        parser.print_usage()
        print exCommand
        sys.exit(1)

    try:
        args = parser.parse_args()
    except IOError, msg:
        parser.error(str(msg))

    directory = args.directory
    forwardReads = args.forward
    reverseReads = args.reverse
    sampleNames = args.samples
    usr = args.usr
    workPath = args.path
    formatDB = args.formatdb
    priority = args.priority

    # Check if all arguments have been entered
    if directory == None:
        print "\nNo directory for query files entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if forwardReads == None:
        print "\nNo forward reads files entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if reverseReads == None:
        print "\nNo reverse reads files enetered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if sampleNames == None:
        print "\nNo sample names entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if usr == None:
        print "\nNo user name entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if workPath == None:
        print "\nNo path to working directory entered!!!\n"
        parser.print_usage()
        print exCommand
        sys.exit(1)

    if formatDB == None:
        formatDB = "y"

    if priority == None:
        priority = "n"

    # Send arguments to start function
    start(directory, forwardReads, reverseReads, sampleNames, usr, workPath,
          formatDB, priority)


main()
